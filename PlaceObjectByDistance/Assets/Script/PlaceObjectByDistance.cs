﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceObjectByDistance : MonoBehaviour {

    public GameObject Path;
    List<Transform> pathList;

 

    // Use this for initialization
    void Start ()
    {
        PointManager wayPoints = Path.GetComponent<PointManager>();
        pathList = wayPoints.WayPoints;

        int counter = 0;
        for (int index = 0; index < pathList.Count - 1; index++)
        {

            Transform pathPoint01 = pathList[index];
            Transform pathPoint02 = pathList[index + 1];


            Vector3 vecBetween = pathPoint02.position - pathPoint01.position;

            float distance = vecBetween.magnitude;
            
            float boxSpacing = 0.5f;

            for (float distFromStart = -0.25f; distFromStart < distance - boxSpacing / 2; distFromStart += boxSpacing)
            {
                //Create a new cube
                GameObject newCube = GameObject.CreatePrimitive(PrimitiveType.Cube);

                //Set the scale
                newCube.transform.localScale = new Vector3(0.5f, 0.5f, 1.5f);

                //?
                PointFollower pointFollowScript = newCube.AddComponent<PointFollower>();

                //Set the target

                pointFollowScript.SetTargetPathPoint(index);
                pointFollowScript.ChangeLocation();

                //Placement
                newCube.transform.position = pathPoint01.position + vecBetween.normalized * distFromStart;


                //Add to counter for each box placed
                counter++;


            }


            if (index + 1 == pathList.Count - 1)
            {
                pathPoint01 = pathList[index + 1];
                pathPoint02 = pathList[0];


                vecBetween = pathPoint02.position - pathPoint01.position;

                distance = vecBetween.magnitude;

                boxSpacing = 0.5f;

                for (float distFromStart = -0.25f; distFromStart < distance - boxSpacing / 2; distFromStart += boxSpacing)
                {
                    //Create a new cube
                    GameObject newCube = GameObject.CreatePrimitive(PrimitiveType.Cube);

                    //Set the scale
                    newCube.transform.localScale = new Vector3(0.5f, 0.5f, 1.5f);

                    //?
                    PointFollower pointFollowScript = newCube.AddComponent<PointFollower>();

                    //Set the target
                    pointFollowScript.SetTargetPathPoint(0);


                    //Placement
                    newCube.transform.position = pathPoint01.position + vecBetween.normalized * distFromStart;


                    //Add to counter for each box placed
                    counter++;


                }
            }
 
        }
       

        Debug.Log("Boxes placed: " + counter);
 

    }




    // Update is called once per frame
    void Update ()
    {
		
	}
}
