﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHolder : MonoBehaviour 
{
	void OnTriggerEnter(Collider aCol)
	{
		aCol.transform.parent = this.gameObject.transform;
	}

	void OnTriggerExit(Collider aCol)
	{
		aCol.transform.parent = null;
	}

 
}
