﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class PointManager : MonoBehaviour {


	public List<Transform> WayPoints = new List<Transform>();

	private int index;
	void Start ()
	{
        index = 0;
    }
	
 
	void Update ()
	{

		Transform[] transforms = GetComponentsInChildren<Transform>();

		if (transforms.Length > 0)
		{
			WayPoints.Clear();

			index = 0;

			foreach (Transform t in transforms)
			{
				if (t != this.transform)
				{
					index++;
				}

                if (index == 0)
                {
                    t.name = "Path Container";
                }
                else
                {
				    t.name = "Path Point_" + index;
                }


				WayPoints.Add(t);

			}
		}
	}



	void OnDrawGizmos()
	{
		Gizmos.color = Color.green;
		foreach (Transform t in WayPoints)
		{
			Gizmos.DrawSphere(t.position, 0.25f);
		}

		Gizmos.color = Color.red;
		for (int index = 0; index < WayPoints.Count - 1; index++)
		{
			Gizmos.DrawLine(WayPoints[index].position, WayPoints[index + 1].position);

			Gizmos.DrawLine(WayPoints[0].position, WayPoints[WayPoints.Count - 1].position);
		}
	}

		   
			


}
