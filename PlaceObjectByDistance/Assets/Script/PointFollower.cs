﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointFollower : MonoBehaviour {

    //public GameObject movingPlatform;
    private Vector3 newPosition;
    public GameObject Path;
    List<Transform> localList;

    int currentPosition;
    bool movingForward;

    bool isMoving;

    // Use this for initialization
    private void Awake ()
    {

        Path = GameObject.FindGameObjectWithTag("Path");

        //currentPosition = 0;
        PointManager wayPoints = Path.GetComponent<PointManager>();
        localList = wayPoints.WayPoints;
        isMoving = false;

    }
       
    private void Start()
    {
      //  Invoke("ChangeLocation", 0.0f);
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        Debug.DrawLine(this.transform.position, newPosition, Color.blue);
        this.transform.position = Vector3.MoveTowards(this.transform.position, newPosition, 0.5f * Time.deltaTime);
        
        if (this.transform.position == newPosition)
        {
            isMoving = false;
            Invoke("ChangeLocation", 0.0f);
        }


    }
       

    public void ChangeLocation()
    {
        isMoving = true; 

        if (currentPosition < localList.Count - 1)
        {
            currentPosition++;
            newPosition = localList[currentPosition].position;
        }
        else
        {
            currentPosition = 0;
            newPosition = localList[currentPosition].position;
        }

    }
        
    public void SetTargetPathPoint(int indexPosition)
    {
        
        //currentPosition = indexPosition;
        if ((indexPosition < localList.Count - 1) || (indexPosition == 0))
        {
            currentPosition = indexPosition;
            newPosition = localList[currentPosition].position;
        }
        else
        {
            currentPosition = 0;
            newPosition = localList[currentPosition].position;
        }
    }

}
